import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
    selector: 'landingPage-component',
    templateUrl: './landingPage.template.html'
})

export class LandingPageComponent implements OnInit{
    public title: string;

    constructor(){
        this.title = "Formula Engine";
    }

    ngOnInit(){
        $('.pushpin-container').pushpin({
            top: $('.pushpin-container').offset().top
        });
        $('.scrollspy').scrollSpy({
            scrollOffset: 0
        });
        $(".button-collapse").sideNav({
            closeOnClick: true
        });
        $('.carousel.carousel-slider').carousel({
            fullWidth: true
        });
    }
}